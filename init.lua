-- This code was modified from TenPlus1's Lucky Block mod. His mod has a lightning staff item that you can only get as a
-- rare drop from his lucky blocks. This mod makes the lightning staff a standalone mod.

minetest.register_tool("lightning_wand:lightning_wand", {
	description = "Lightning Wand",
	inventory_image = "lightning_wand.png",
	range = 10,
	groups = { tool = 1 },
	wield_scale = 2,

	on_use = function(itemstack, user, pointed_thing)
		if not pointed_thing then return end

		if pointed_thing.type == "object" then
			lightning.strike(pointed_thing.ref:get_pos())
		elseif pointed_thing.type == "node" then
			lightning.strike(pointed_thing.above)
		else
			return
		end

		if not minetest.is_creative_enabled(user:get_player_name()) then
			-- The wand has 100? uses
        	itemstack:add_wear(65535 / 100)
		end

		-- Without making sure the player is alive with user:get_hp() > 0, the player can duplicate the wand by committing
		-- suicide with the wand, b/c they'll get the wand back after death
		if user:get_hp() > 0 then
        	return itemstack
		end
    end
})

-- MineClone2 crafting recipe
--minetest.register_craft({
--    recipe = {
--        { "", "", "mcl_amethyst:amethyst_block" },
--        { "", "mcl_lightning_rods:rod", "" },
--        { "mcl_lightning_rods:rod", "", "" }
--    },
--    output = "lightning_wand:lightning_wand"
--})
-- Minetest game crafting recipe
--minetest.register_craft({
--    recipe = {
--        { "", "", "default:meselamp" },
--        { "", "default:diamond", "" },
--        { "default:diamond", "", "" }
--    },
--    output = "lightning_wand:lightning_wand"
--})
